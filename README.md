# Google Chrome Browser Developer Build Headless VNC
===

#### To build it:

```bash
docker build -t="chromeappupdates/chrome-dev-vnc" bitbucket.org/chromeappupdates/chrome-dev-vnc
```

### To Run it

```bash
docker run -it --rm -p 5901:5901 chromeappupdates/chrome-dev-vnc
USER=root vncserver :1 -geometry 1280x800 -depth 24
```

### Now Connect using port 5901 in your preferred VNC (client) app

```Dockerfile
# Pull base image.
FROM ubuntu-desktop

# Install Chrome Dev Build (builds weekly approx, not nightly)
RUN \
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - 
&& \
sudo sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google-chrome.list' && \
  apt-get update && \
  apt-get install -y google-chrome-unstable && \
  rm -rf /var/lib/apt/lists/*

# Define working directory.
WORKDIR /data

# Define default command.
CMD ["bash"]

# Expose ports.
EXPOSE 5901
```